# Lightbike
This small project aims at creating a simple game and graphics tool to allow users to test game AIs against one another.
The idea is that users can train/code their own game to compete against one another online, with the end goal of "evolving" user models as they compete and train against other models.

## Example UI
![AI Performance Review](light_bike.gif)  
The simple UI lets users review their bots performance over hundreds of games. 
They can adjust playback speed, board gridlines, and their own custom generated plot objects (as shown here in yellow as
its A* pathfinding).

Ultimately the game needs some modification, as a simple Tron game is too easily solvable with
modern pathfinding methods.