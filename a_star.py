import numpy as np
import lightbike_fight as lbf
from pathfinding.core.grid import Grid
from pathfinding.finder.a_star import AStarFinder
from pathfinding.core.diagonal_movement import DiagonalMovement
# from matplotlib import animation
import random
import seaborn as sns
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle

def random_safe(board, my_position, open):
    #print("Executing random safe")
    pos_right = (my_position[0]+1,my_position[1])
    pos_left = (my_position[0]-1,my_position[1])
    pos_down = (my_position[0],my_position[1]-1)
    pos_up = (my_position[0],my_position[1]+1)

    up =   board[pos_up[::-1]]
    down = board[pos_down[::-1]]
    left = board[pos_left[::-1]]
    right =board[pos_right[::-1]]


    possible_choices = []
    if up == open:
        possible_choices.append('up')
    if down == open:
        possible_choices.append('down')
    if left == open:
        possible_choices.append('left')
    if right == open:
        possible_choices.append('right')
    if possible_choices:
        ret = random.choice(possible_choices)
    else:
        ret = 'up'
    return(ret)

def a_star(state):
    key = 'a_star'
    blocked = 0
    open = 1
    my_position = state['player_info'][key]['position']
    op_position = state['player_info']['player0']['position']
    try:
        op_last = state['player_info']['player0']['locations'][-2]
    except IndexError:
        op_last = op_position
    num_players = state['game_info']['num_players']
    x_bound = state['game_info']['x_bound']
    y_bound = state['game_info']['y_bound']

    board = np.full((y_bound+2, x_bound+2),open)
    for player in state['player_info'].values():
        for location in player['locations']:
            board[location[1],location[0]] = blocked

    for x in range(0, x_bound+2):
        board[0,x] = blocked
        board[y_bound+1,x] = blocked
    for y in range(0, y_bound+2):
        board[y,0] = blocked
        board[y,x_bound+1] = blocked

    blocked_cells = []
    for index, cell in np.ndenumerate(board):
        if cell == blocked:
            blocked_cells.append(index)

    direction = (np.array(op_position) - np.array(op_last))
    target = op_position + direction

    grid = Grid(matrix=board.tolist())
    start = grid.node(my_position[0], my_position[1])
    end = grid.node(target[0], target[1])

    finder = AStarFinder(diagonal_movement=DiagonalMovement.never)
    path, runs = finder.find_path(start, end, grid)

    #print(grid.grid_str(path=path, start=start, end=end))

    board = np.full((y_bound+2, x_bound+2),np.nan)

    try:
        for location in path:
            board[location[1], location[0]] = 0.80
    except IndexError:
        pass

    for location in state['player_info']['a_star']['locations']:
            board[location[1],location[0]] = 0.2
    for location in state['player_info']['player0']['locations']:
            board[location[1],location[0]] = 0


    for x in range(0, x_bound+2):
        board[0,x] = 1
        board[y_bound+1,x] = 1
    for y in range(0, y_bound+2):
        board[y,0] = 1
        board[y,x_bound+1] = 1

    #Game plotting
    #fig = plt.figure(figsize=(8,8))

    #plt.clf()
    #ax = sns.heatmap(board)
    #ax.add_patch(Rectangle(my_position, 1, 1, fill=False, edgecolor='green', lw=3))
    #ax.add_patch(Rectangle(target, 1, 1, fill=False, edgecolor='red', lw=3))
    #ax.invert_yaxis()

    #ax.add_patch(Rectangle(target, 1, 1, fill=False, edgecolor='green', lw=3))
    #anim = animation.FuncAnimation(fig, animate, repeat = False)
    plt.show()
    try:
        next_position = path[1]
        if next_position in blocked_cells:
            return(random_safe(board, my_position, open))
        else:
            direction = (next_position[0]-my_position[0] , next_position[1] - my_position[1] )
            if direction == (0, 1):
                return('up')
            elif direction == (0, -1):
                return('down')
            elif direction == (1, 0):
                return('right')
            else:
                return('left')
    except IndexError:
        return(random_safe(state))

if __name__ == '__main__':

    bots = {
        'player0': lbf.checker('player0').update,
        #'player1': lbf.checker('player1').update,
        'a_star': a_star,
    }
    results = lbf.test_bots(bots, 100, plot=True)
