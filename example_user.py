import lightbike_fight as lbf
import pickle

from pathfinding.core.grid import Grid
from pathfinding.finder.a_star import AStarFinder
from pathfinding.core.diagonal_movement import DiagonalMovement
import numpy as np
import random


class my_bot:
    def __init__(self, key):
        self.plot_objects = None
        self.key = key
        self.last = None

    def get_spots(self, locations, color):
        #color = 'c'
        spots = []
        for location in locations:
            spots.append({
                'pos': (location[1], location[0],),
                'pen': {'color': color, 'width': 0},
                'brush': color,
                'size': 0.5,
            })
        return spots

    def update(self, state):
        my_position = state['player_info'][self.key]['position']
        num_players = state['game_info']['num_players']
        x_bound = state['game_info']['x_bound']
        y_bound = state['game_info']['y_bound']

        board = np.full((y_bound + 2, x_bound + 2), np.nan)

        for player in state['player_info'].values():
            for location in player['locations']:
                board[location[1], location[0]] = 1

        for x in range(0, x_bound + 2):
            board[-1, x] = 1
            board[y_bound + 1, x] = 1
        for y in range(0, y_bound + 2):
            board[y, -1] = 1
            board[y, x_bound + 1] = 1

        pos_right = (my_position[0] + 1, my_position[1])
        pos_left = (my_position[0] - 1, my_position[1])
        pos_down = (my_position[0], my_position[1] - 1)
        pos_up = (my_position[0], my_position[1] + 1)

        up = board[pos_up[::-1]]
        down = board[pos_down[::-1]]
        left = board[pos_left[::-1]]
        right = board[pos_right[::-1]]

        possible_choices = []
        choices_locations = []

        move2pos={
            'up': pos_up,
            'down': pos_down,
            'left': pos_left,
            'right': pos_right,
        }

        if up not in [-1, 1]:
            possible_choices.append('up')
            choices_locations.append(pos_up)
        if down not in [-1, 1]:
            possible_choices.append('down')
            choices_locations.append(pos_down)
        if left not in [-1, 1]:
            possible_choices.append('left')
            choices_locations.append(pos_left)
        if right not in [-1, 1]:
            possible_choices.append('right')
            choices_locations.append(pos_right)



        if possible_choices:
            ret = random.choice(possible_choices)
        else:
            ret = 'up'

        self.plot_objects = self.get_spots(choices_locations, 'c')
        self.plot_objects.append(self.get_spots([move2pos[ret]], 'g')[0])
        #self.plot_objects.append(self.get_spots(move2pos[ret], 'r')[0])
        #self.plot_objects.append(self.get_spots([ret], 'y'))
        self.last = ret
        return (ret)


class AStar:
    def __init__(self, name, opponent_name):
        self.key = name
        self.op_key = opponent_name
        self.last = None
        self.op_last = None
        self.finder = AStarFinder(diagonal_movement=DiagonalMovement.never)

    def get_spots(self, locations, color):
        #color = 'c'
        spots = []
        for location in locations:
            spots.append({
                'pos': (location[1], location[0],),
                'pen': {'color': color, 'width': 0},
                'brush': color,
                'size': 0.5,
            })
        return spots

    # Executes a randomly safe move
    def random_safe(self, state):
        my_position = state['player_info'][self.key]['position']
        num_players = state['game_info']['num_players']
        x_bound = state['game_info']['x_bound']
        y_bound = state['game_info']['y_bound']

        board = np.full((y_bound + 2, x_bound + 2), np.nan)

        for player in state['player_info'].values():
            for location in player['locations']:
                board[location[1], location[0]] = 1

        for x in range(0, x_bound + 2):
            board[-1, x] = 1
            board[y_bound + 1, x] = 1
        for y in range(0, y_bound + 2):
            board[y, -1] = 1
            board[y, x_bound + 1] = 1

        pos_right = (my_position[0] + 1, my_position[1])
        pos_left = (my_position[0] - 1, my_position[1])
        pos_down = (my_position[0], my_position[1] - 1)
        pos_up = (my_position[0], my_position[1] + 1)

        up = board[pos_up[::-1]]
        down = board[pos_down[::-1]]
        left = board[pos_left[::-1]]
        right = board[pos_right[::-1]]

        possible_choices = []
        choices_locations = []

        if up not in [-1, 1]:
            possible_choices.append('up')
            choices_locations.append(pos_up)
        if down not in [-1, 1]:
            possible_choices.append('down')
            choices_locations.append(pos_down)
        if left not in [-1, 1]:
            possible_choices.append('left')
            choices_locations.append(pos_left)
        if right not in [-1, 1]:
            possible_choices.append('right')
            choices_locations.append(pos_right)

        if possible_choices:
            ret = random.choice(possible_choices)
            self.plot_objects = self.get_spots(choices_locations, 'r')
        else:
            ret = 'up'
        self.last = ret
        return ret

    def update(self, state):
        blocked = 0
        open_tile = 1
        my_position = state['player_info'][self.key]['position']
        op_position = state['player_info'][self.op_key]['position']
        try:
            self.op_last = state['player_info'][self.op_key]['locations'][-2]
        except IndexError:
            self.op_last = op_position
        x_bound = state['game_info']['x_bound']
        y_bound = state['game_info']['y_bound']

        board = np.full((y_bound + 2, x_bound + 2), open_tile)
        for player in state['player_info'].values():
            for location in player['locations']:
                board[location[1], location[0]] = blocked

        for x in range(-1, x_bound + 2):
            board[-1, x] = blocked
            board[y_bound + 1, x] = blocked
        for y in range(-1, y_bound + 2):
            board[y, -1] = blocked
            board[y, x_bound + 1] = blocked

        blocked_cells = []
        for index, cell in np.ndenumerate(board):
            if cell == blocked:
                blocked_cells.append(index)

        direction = (np.array(op_position) - np.array(self.op_last))
        target = op_position + direction

        grid = Grid(matrix=board.tolist())
        start = grid.node(my_position[0], my_position[1])

        try:
            end = grid.node(target[0], target[1])
            path, runs = self.finder.find_path(start, end, grid)

        except IndexError:
            target = op_position
            try:
                end = grid.node(target[0], target[1])
                path, runs = self.finder.find_path(start, end, grid)
            except IndexError:
                print('Target off grid')
                return (self.random_safe(state))



        #print(grid.grid_str(path=path, start=start, end=end))
        try:
            self.plot_objects = self.get_spots(path, 'y')
            next_position = path[1]
            if next_position in blocked_cells:
                return (self.random_safe(state))
            else:
                direction = (next_position[0] - my_position[0], next_position[1] - my_position[1])
                if direction == (0, 1):
                    return ('up')
                elif direction == (0, -1):
                    return ('down')
                elif direction == (1, 0):
                    return ('right')
                else:
                    return ('left')
        except IndexError:
            return (self.random_safe(state))


if __name__ == '__main__':
    bots = {
        'a*': AStar('a*', 'player1'),
        'player1': lbf.checker('player1'),
    }

    results = lbf.test_bots(bots, 100, 20, 20)
    pickle.dump(results, open("results.p", "wb"))
    lbf.display_results("results.p")

