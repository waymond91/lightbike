import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
from matplotlib import colors
from matplotlib.figure import Figure
from matplotlib.backends.backend_qt5agg import (
    FigureCanvas, NavigationToolbar2QT as NavigationToolbar)
from matplotlib.backends.qt_compat import QtCore, QtWidgets, is_pyqt5
import pickle
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5 import QtWidgets
from PyQt5.QtCore import *
from PyQt5 import QtCore
import os
import sys

results = pickle.load(open('results.p', 'rb'))

class lb_plot():
    def __init__(self, states, user_objects = None):
        self.last_index = 0
        self.states = states
        self.rendered_colors = ['floralwhite','black']
        self.rendered_objects = ['Open Square', 'Game Border']
        self.player_colors = [
            'lightseagreen',
            'maroon',
            'darkorange',
            'darkolivegreen',
            'indianred',
            'darkslategray',
            'rebeccapurple',
        ]
        #self.rendered_objects = ['Open Square', 'Game Border']

        self.y_bound = states[0]['game_info']['y_bound']
        self.x_bound = states[0]['game_info']['x_bound']

        # Initialize a blank board
        color_index = 0
        self.img = np.full((self.y_bound+2, self.x_bound+2),color_index)
        color_index += 1

        for x in range(0, self.x_bound+2):
            self.img[0,x] = color_index
            self.img[self.y_bound+1,x] = color_index
        for y in range(0, self.y_bound+2):
            self.img[y,0] = color_index
            self.img[y,self.x_bound+1] = color_index
        color_index += 1

        player_index = 0
        for player_key, player in states[0]['player_info'].items():
            self.rendered_colors.append(self.player_colors[player_index])
            self.rendered_objects.append(player_key)
            position = player['position']
            self.img[position[1], position[0]] = color_index
            player_index += 1
            color_index += 1

        num_objects = len(self.rendered_objects)
        self.cmap = colors.ListedColormap(self.rendered_colors)
        self.bounds = np.linspace(0, num_objects, num_objects+1)
        self.norm = colors.BoundaryNorm(self.bounds, self.cmap.N)

        #ax = sns.heatmap(self.img, cmap=self.cmap, norm=self.norm, annot=False, linewidths=0.0)
        #self.cbar = ax.collections[0].colorbar
        #self.cbar.set_ticks(self.bounds+0.5)
        #self.cbar.set_ticklabels(self.rendered_objects)
        plt.close()

    def clear(self, states, user_objects = None):
        self.__init__(states, user_objects=user_objects)

    def update(self, states, _ax, i):
        diff = i - self.last_index
        if diff < 0:
            for j in range(1,abs(diff)+1,1):
                for player_key, player in states[i+j]['player_info'].items():
                    position = player['position']
                    self.img[position[1], position[0]] = 0
        else:
            for j in range(0,abs(diff)+1,1):
                color_index = 2
                player_index = 0
                for player_key, player in states[self.last_index+j]['player_info'].items():
                    position = player['position']
                    self.img[position[1], position[0]] = color_index
                    player_index += 1
                    color_index += 1
        ax = sns.heatmap(self.img, cmap=self.cmap, norm=self.norm, annot=False, linewidths=0.0, cbar=False, ax=_ax)
        patches = []
        state = states[i]
        for player in state['player_info'].values():
            patches.append(Rectangle(player['position'], 1, 1, fill=False, edgecolor='black', lw=3))
        for patch in patches:
            ax.add_patch(patch)

        ax.invert_yaxis()
        self.last_index = i

class App(QWidget):

    def __init__(self):
        super().__init__()
        self.title = 'Unnatural Selection'
        self.left = 10
        self.top = 10
        self.width = 320
        self.height = 550
        self.initUI()

class ApplicationWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()
        self._main = QtWidgets.QWidget()
        self.setCentralWidget(self._main)
        #layout = QtWidgets.QVBoxLayout(self._main)

        self.state_index  = 0
        self.game_index = 0
        self.states = results[self.game_index]['states']

        self.l1 = QLabel()
        self.l1.setText("Move No: "+str(self.state_index))

        self.sl = QSpinBox()
        self.sl.setMinimum(0)
        self.sl.setMaximum(len(self.states)-1)
        self.sl.setSingleStep(1)
        self.sl.valueChanged.connect(self.sl_handler)

        self.l2 = QLabel()
        self.l2.setText("Game No: "+ str(self.game_index))

        self.sp = QSpinBox()
        self.sp.setMinimum(0)
        self.sp.setMaximum(len(results))
        self.sp.setSingleStep(1)
        self.sp.valueChanged.connect(self.sp_handler)

        self.playing=False
        self.pb = QPushButton()
        self.pb.setText('Play')
        self.pb.clicked.connect(self.pb_handler)

        self.timer = QTimer()
        self.timer.timeout.connect(self.timer_handler)

        self.heatplot = lb_plot(self.states)

        dynamic_canvas = FigureCanvas(Figure(figsize=(8, 8)))

        self._dynamic_ax = dynamic_canvas.figure.subplots()
        self._update_canvas()

        layout = QGridLayout(self._main)
        layout.addWidget(dynamic_canvas,0,0 ,1,0)
        layout.addWidget(self.l1,1,0)
        layout.addWidget(self.sl,1,1)
        layout.addWidget(self.l2,2,0)
        layout.addWidget(self.sp,2,1)
        layout.addWidget(self.pb,3,0, 1,0)


    def _update_canvas(self):
        state = self.states[self.state_index]
        self._dynamic_ax.clear()
        #qt_plot(state, self._dynamic_ax)
        self.heatplot.update(self.states, self._dynamic_ax, self.sl.value())
        self._dynamic_ax.figure.canvas.draw()


    def sl_handler(self):
        self.state_index = self.sl.value()
        self.l1.setText("Move No: "+str(self.state_index))
        #print(self.state_index)
        self._update_canvas()

    def sp_handler(self):
        self.game_index = self.sp.value()
        self.sl.setValue(0)
        self.states = results[self.game_index]['states']
        self.heatplot.clear(self.states)
        self.sl.setMaximum(len(self.states)-1)
        self.l2.setText("Game No: "+ str(self.game_index))
        self._update_canvas()


    def pb_handler(self):
        if self.playing:
            self.playing = False
            self.timer.stop()
            self.pb.setText('Play')
        else:
            if self.sl.value() >= len(self.states) -1:
                self.sl.setValue(0)
            self.playing = True
            self.timer.start(1)
            self.pb.setText('Pause')

    def timer_handler(self):
        #print(self.sl.value(), len(self.states))
        self.sl.setValue(self.sl.value()+1)
        self._update_canvas()
        if self.sl.value() >= len(self.states)-1:
            #print('stopped')
            self.timer.stop()
            self.pb.setText('Play')
            self.playing = False


def qt_plot(state, ax ,plot_objects = None):

    player_colors = [
        'indianred',
        'maroon',
        'darkorange',
        'darkolivegreen',
        'lightseagreen',
        'darkslategray',
        'rebeccapurple',
    ]

    rendered_colors = ['black', 'floralwhite']
    rendered_objects = ['Game Border', 'Open Square']

    num_objects = 2 + state['game_info']['num_players']
    y_bound = state['game_info']['y_bound']
    x_bound = state['game_info']['x_bound']

    img = np.full((y_bound+2, x_bound+2),1)

    for x in range(0, x_bound+2):
        img[0,x] = 0
        img[y_bound+1,x] = 0
    for y in range(0, y_bound+2):
        img[y,0] = 0
        img[y,x_bound+1] = 0

    color_index = 2

    for player_key, player in state['player_info'].items():
        for location in player['locations'][:-1]:
            img[location[1],location[0]] = color_index
        position = player['position']
        img[position[1], position[0]] = color_index
        rendered_colors.append(player_colors[color_index])
        rendered_objects.append(player_key)
        color_index += 1

    if plot_objects:
        num_objects += len(plot_objects)
        for key, values in plot_objects.items():
            rendered_colors.append(values['color'])
            rendered_objects.append(key)
            for location in values['locations']:
                img[location[1],location[0]] = color_index
            color_index +=1

    cmap = colors.ListedColormap(rendered_colors)
    bounds = np.linspace(0, num_objects, num_objects+1)
    norm = colors.BoundaryNorm(bounds, cmap.N)


    ax = sns.heatmap(img, cmap=cmap, norm=norm, annot=False, linewidths=0.0, ax=ax, cbar=False )
    patches = []
    for player in state['player_info'].values():
        #print(player)
        patches.append(Rectangle(player['position'], 1, 1, fill=False, edgecolor='black', lw=3))
        #patches.append(player['position'])
    for patch in patches:
        ax.add_patch(patch)

    #cbar = ax.collections[0].colorbar
    #cbar.set_ticks(bounds+0.5)
    #cbar.set_ticklabels(rendered_objects)
    ax.invert_yaxis()

if __name__ == "__main__":
    qapp = QtWidgets.QApplication(sys.argv)
    app = ApplicationWindow()
    app.show()
    qapp.exec_()
