#! /home/brett/anaconda3/envs/keras/bin/python

import numpy as np
import random
import seaborn as sns
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
from matplotlib import animation
from progress.bar import IncrementalBar
from matplotlib import colors
import copy

class checker:
    def __init__(self, key):
        self.key = key
        self.last = None

    def update(self, state):
        #print(state.keys())
        my_position = state['player_info'][self.key]['position']
        num_players = state['game_info']['num_players']
        x_bound = state['game_info']['x_bound']
        y_bound = state['game_info']['y_bound']

        board = np.full((y_bound+2, x_bound+2),np.nan)

        for player in state['player_info'].values():
            for location in player['locations']:
                board[location[1],location[0]] = 1

        for x in range(0, x_bound+2):
            board[0,x] = 1
            board[y_bound+1,x] = 1
        for y in range(0, y_bound+2):
            board[y,0] = 1
            board[y,x_bound+1] = 1

        pos_right = (my_position[0]+1,my_position[1])
        pos_left = (my_position[0]-1,my_position[1])
        pos_down = (my_position[0],my_position[1]-1)
        pos_up = (my_position[0],my_position[1]+1)

        up =   board[pos_up[::-1]]
        down = board[pos_down[::-1]]
        left = board[pos_left[::-1]]
        right =board[pos_right[::-1]]

        possible_choices = []
        choices_locations = []
        if up not in [-1, 1]:
            possible_choices.append('up')
            choices_locations.append(pos_up)
        if down not in [-1, 1]:
            possible_choices.append('down')
            choices_locations.append(pos_down)
        if left not in [-1, 1]:
            possible_choices.append('left')
            choices_locations.append(pos_left)
        if right not in [-1, 1]:
            possible_choices.append('right')
            choices_locations.append(pos_right)

        if possible_choices:
            ret = random.choice(possible_choices)
        else:
            #print(self.name+' Trapped!')
            ret = 'up'

        to_plot = None
        if choices_locations and self.key == 'player_0':
            to_plot = {
                'checker choices': {
                    'color': 'gold',
                    'locations' : choices_locations,
                }
            }
            #print(to_plot)
        """
        if to_plot:
            game_plot(state, plot_objects=to_plot)
        """
        self.last = ret
        return(ret)

class lightbike:
    dead = False

    opposites = {
        'up':'down',
        'down': 'up',
        'left': 'right',
        'right': 'left',
    }
    def __init__(self, update_func, starting_location):
        self.player_update = update_func
        self.location = starting_location
        self.direction = None

    def update(self, state):
        self.last_direction = self.direction
        self.direction = self.player_update(state)

        if self.last_direction == self.opposites[self.direction]:
            self.direction = self.last_direction

        return(self.direction)

class lightbike_fight:
    def __init__(self, bots):
        self.count = 0
        self.x_bound = 55
        self.y_bound = 55
        self.players = {}
        self.state = {'game_info': {}, 'player_info': {}}
        self.states = []
        self.results = {}
        self.losers = []
        self.blocked_locations = []
        self.num_players = len(bots)

        self.state['game_info'].update({
            'x_bound': self.x_bound,
            'y_bound': self.y_bound,
            'num_players': self.num_players,
            'num_moves': 0,
        })

        for bot, bot_function in bots.items():
            x_start = random.randint(0, self.x_bound)
            y_start = random.randint(0, self.y_bound)

            self.players.update({
                bot: lightbike(bot_function, [x_start, y_start])
            })

            self.state['player_info'].update({
                bot: {
                    'locations': [],
                    'position' : [x_start, y_start],
                }
            })

    def update(self):
        for player_key, player in self.players.items():
            if player.dead == False:
                player.update(self.state)
                if player.direction == 'up':
                    player.location[1] += 1
                elif player.direction == 'down':
                    player.location[1] -= 1
                elif player.direction == 'right':
                    player.location[0] += 1
                elif player.direction == 'left':
                    player.location[0] -= 1
        for player_key, player in self.players.items():
            if player.dead == False:
                self.state['player_info'][player_key]['position'] = player.location
                self.state['player_info'][player_key]['locations'].append(player.location[:])
        self.check_for_losers()

        for player in self.state['player_info'].values():
            if len(player['locations']) > 2:
                self.blocked_locations.append(copy.deepcopy(player['locations'][-2]))

        self.states.append(copy.deepcopy(self.state))

    def check_for_losers(self):
        #blocked_locations = []
        """
        for player in self.state['player_info'].values():
            #print(player)
            for location in player['locations'][:-1]:
                blocked_locations.append(location)
        """
        for player_key, player in self.players.items():
            if player.dead == False:
                if player.location[0] > self.x_bound or player.location[0]< 0:
                    player.dead = True
                if player.location[1] > self.y_bound or player.location[1]< 0 :
                    player.dead = True

                if player.location in self.blocked_locations:
                    player.dead = True
                if player.dead == True:
                    #print(player_key+" died: ", player.location)
                    if player_key not in self.losers:
                        self.losers.append(player_key)
                        rank = self.num_players - self.losers.index(player_key)
                        self.results.update({
                            player_key:{
                                'rank': rank,
                                'moves': self.count,
                                #'state': self.state,

                            }
                        })
        self.count += 1

def qt_plot(state, ax ,plot_objects = None):

    player_colors = [
        'indianred',
        'maroon',
        'darkorange',
        'darkolivegreen',
        'lightseagreen',
        'darkslategray',
        'rebeccapurple',
    ]

    rendered_colors = ['black', 'floralwhite']
    rendered_objects = ['Game Border', 'Open Square']

    num_objects = 2 + state['game_info']['num_players']
    y_bound = state['game_info']['y_bound']
    x_bound = state['game_info']['x_bound']

    img = np.full((y_bound+2, x_bound+2),1)

    for x in range(0, x_bound+2):
        img[0,x] = 0
        img[y_bound+1,x] = 0
    for y in range(0, y_bound+2):
        img[y,0] = 0
        img[y,x_bound+1] = 0

    color_index = 2

    for player_key, player in state['player_info'].items():
        for location in player['locations'][:-1]:
            img[location[1],location[0]] = color_index
        position = player['position']
        img[position[1], position[0]] = color_index
        rendered_colors.append(player_colors[color_index])
        rendered_objects.append(player_key)
        color_index += 1

    if plot_objects:
        num_objects += len(plot_objects)
        for key, values in plot_objects.items():
            #print(key, values)
            rendered_colors.append(values['color'])
            rendered_objects.append(key)
            for location in values['locations']:
                img[location[1],location[0]] = color_index
            color_index +=1

    #plt.figure(figsize=(6,6))
    cmap = colors.ListedColormap(rendered_colors)
    bounds = np.linspace(0, num_objects, num_objects+1)
    #bounds=[-0.5, 0.5, 1.5, 2.5, 3.5]

    norm = colors.BoundaryNorm(bounds, cmap.N)
    #heatmap = plt.pcolor(np.array(img), cmap=cmap, norm=norm)
    #plt.colorbar(ax, ticks=[0, 1, 2, 3])

    ax = sns.heatmap(img, cmap=cmap, norm=norm, annot=False, linewidths=0.0, ax=ax, cbar=False )
    patches = []
    for player in state['player_info'].values():
        #print(player)
        patches.append(Rectangle(player['position'], 1, 1, fill=False, edgecolor='black', lw=3))
        #patches.append(player['position'])
    for patch in patches:
        ax.add_patch(patch)

    #cbar = ax.collections[0].colorbar
    #cbar.set_ticks(bounds+0.5)
    #cbar.set_ticklabels(rendered_objects)
    ax.invert_yaxis()
    #return(ax)
    #plt.show()

def game_plot(state, ax = None,plot_objects = None):

    player_colors = [
        'indianred',
        'maroon',
        'darkorange',
        'darkolivegreen',
        'lightseagreen',
        'darkslategray',
        'rebeccapurple',
    ]

    rendered_colors = ['black', 'floralwhite']
    rendered_objects = ['Game Border', 'Open Square']

    num_objects = 2 + state['game_info']['num_players']
    y_bound = state['game_info']['y_bound']
    x_bound = state['game_info']['x_bound']

    img = np.full((y_bound+2, x_bound+2),1)

    for x in range(0, x_bound+2):
        img[0,x] = 0
        img[y_bound+1,x] = 0
    for y in range(0, y_bound+2):
        img[y,0] = 0
        img[y,x_bound+1] = 0

    color_index = 2

    for player_key, player in state['player_info'].items():
        for location in player['locations'][:-1]:
            img[location[1],location[0]] = color_index
        position = player['position']
        img[position[1], position[0]] = color_index
        rendered_colors.append(player_colors[color_index])
        rendered_objects.append(player_key)
        color_index += 1

    if plot_objects:
        num_objects += len(plot_objects)
        for key, values in plot_objects.items():
            #print(key, values)
            rendered_colors.append(values['color'])
            rendered_objects.append(key)
            for location in values['locations']:
                img[location[1],location[0]] = color_index
            color_index +=1

    #plt.figure(figsize=(6,6))
    cmap = colors.ListedColormap(rendered_colors)
    bounds = np.linspace(0, num_objects, num_objects+1)
    #bounds=[-0.5, 0.5, 1.5, 2.5, 3.5]

    norm = colors.BoundaryNorm(bounds, cmap.N)
    #heatmap = plt.pcolor(np.array(img), cmap=cmap, norm=norm)
    #plt.colorbar(ax, ticks=[0, 1, 2, 3])

    ax = sns.heatmap(img, cmap=cmap, norm=norm, annot=False, linewidths=.06250, )
    patches = []
    for player in state['player_info'].values():
        #print(player)
        patches.append(Rectangle(player['position'], 1, 1, fill=False, edgecolor='black', lw=3))
        #patches.append(player['position'])
    for patch in patches:
        ax.add_patch(patch)

    cbar = ax.collections[0].colorbar
    cbar.set_ticks(bounds+0.5)
    cbar.set_ticklabels(rendered_objects)
    ax.invert_yaxis()
    #return(ax)
    plt.show()

class lb_plot:
    def __init__(self, states, user_objects = None):
        self.states = states
        self.rendered_colors = ['floralwhite','black']
        self.rendered_objects = ['Open Square', 'Game Border']
        self.player_colors = [
            'lightseagreen',
            'maroon',
            'darkorange',
            'darkolivegreen',
            'indianred',
            'darkslategray',
            'rebeccapurple',
        ]
        #self.rendered_objects = ['Open Square', 'Game Border']

        self.y_bound = states[0]['game_info']['y_bound']
        self.x_bound = states[0]['game_info']['x_bound']

        # Initialize a blank board
        color_index = 0
        self.img = np.full((self.y_bound+2, self.x_bound+2),color_index)
        color_index += 1

        for x in range(0, self.x_bound+2):
            self.img[0,x] = color_index
            self.img[self.y_bound+1,x] = color_index
        for y in range(0, self.y_bound+2):
            self.img[y,0] = color_index
            self.img[y,self.x_bound+1] = color_index
        color_index += 1

        player_index = 0
        for player_key, player in states[0]['player_info'].items():
            self.rendered_colors.append(self.player_colors[player_index])
            self.rendered_objects.append(player_key)
            position = player['position']
            self.img[position[1], position[0]] = color_index
            player_index += 1
            color_index += 1

        num_objects = len(self.rendered_objects)
        self.cmap = colors.ListedColormap(self.rendered_colors)
        self.bounds = np.linspace(0, num_objects, num_objects+1)
        self.norm = colors.BoundaryNorm(self.bounds, self.cmap.N)

        self.ax = sns.heatmap(self.img, cmap=self.cmap, norm=self.norm, annot=False, linewidths=0.0)
        #self.cbar = self.ax.collections[0].colorbar
        #self.cbar.set_ticks(self.bounds+0.5)
        #self.cbar.set_ticklabels(self.rendered_objects)
        plt.close()

    def update(self, i, states):
        #plt.clf()
        if i == 0:
            return
        color_index = 2
        player_index = 0
        for player_key, player in states[i]['player_info'].items():
            position = player['position']
            self.img[position[1], position[0]] = color_index
            player_index += 1
            color_index += 1
        self.ax = sns.heatmap(self.img, cmap=self.cmap, norm=self.norm, annot=False, linewidths=0.0, cbar=False)
        #self.cbar = self.ax.collections[0].colorbar
        #self.cbar.set_ticks(self.bounds+0.5)
        #self.cbar.set_ticklabels(self.rendered_objects)


def animate(i, states, plot_objects = None):
    state = states[i]
    #print(states[-1])
    player_colors = [
        'lightseagreen',
        'maroon',
        'darkorange',
        'darkolivegreen',
        'indianred',
        'darkslategray',
        'rebeccapurple',
    ]

    rendered_colors = ['floralwhite','black']
    rendered_objects = ['Open Square', 'Game Border']
    num_objects = 2 + state['game_info']['num_players']
    y_bound = state['game_info']['y_bound']
    x_bound = state['game_info']['x_bound']

    color_index = 0
    img = np.full((y_bound+2, x_bound+2),color_index)
    color_index += 1

    for x in range(0, x_bound+2):
        img[0,x] = color_index
        img[y_bound+1,x] = color_index
    for y in range(0, y_bound+2):
        img[y,0] = color_index
        img[y,x_bound+1] = color_index
    color_index += 1

    player_index = 0
    for player_key, player in state['player_info'].items():
        rendered_colors.append(player_colors[player_index])
        rendered_objects.append(player_key)
        for location in player['locations'][:-1]:
            img[location[1],location[0]] = color_index
        position = player['position']
        img[position[1], position[0]] = color_index
        player_index += 1
        color_index += 1


    if plot_objects:
        num_objects += len(plot_objects)
        for key, values in plot_objects.items():
            rendered_colors.append(values['color'])
            rendered_objects.append(key)
            for location in values['locations']:
                img[location[1],location[0]] = color_index
            color_index +=1

    #plt.figure(figsize=(6,6))
    cmap = colors.ListedColormap(rendered_colors)
    bounds = np.linspace(0, num_objects, num_objects+1)
    norm = colors.BoundaryNorm(bounds, cmap.N)
    print(rendered_colors)
    plt.clf()
    ax = sns.heatmap(img, cmap=cmap, norm=norm, annot=False, linewidths=0.0)
    patches = []
    for player in state['player_info'].values():
        patches.append(Rectangle(player['position'], 1, 1, fill=False, edgecolor='black', lw=3))
    for patch in patches:
        ax.add_patch(patch)

    cbar = ax.collections[0].colorbar
    cbar.set_ticks(bounds+0.5)
    cbar.set_ticklabels(rendered_objects)
    ax.invert_yaxis()
    #plt.show()

def test_bots(bot_funcs, num_games, plot = False):
    count = 0
    games = []
    results = []
    for i in range(num_games):
        games.append(lightbike_fight(bot_funcs))

    bar = IncrementalBar('Countdown', max = num_games)
    for game in games:
        while(len(game.losers)<game.num_players-1):
            game.update()
            #game_plot(game.state)
        results.append({
            'ranks': game.results,
            'states' : game.states,
        })
        bar.next()
    bar.finish()

    gp = lb_plot(results[0]['states'])

    fig = plt.figure(figsize=(10,8))
    anim = animation.FuncAnimation(fig, gp.update, fargs=(results[0]['states'],), \
        frames = len(results[0]['states'])-1,repeat = False)
    plt.show()

    return results

def animate_game(states):
    fig = plt.figure(figsize=(10,8))
    anim = animation.FuncAnimation(fig, animate, fargs=(states,), \
        frames = len(states)-1,repeat = False)
    plt.show()

if __name__ == '__main__':

    bots = {
        'player_0': checker('player_0').update,
        'punch_party': checker('punch_party').update,
        'player_1': checker('player_1').update,
        'player_2': checker('player_2').update,
        'player_3': checker('player_3').update,
    }

    results = test_bots(bots, 10, plot=True)
