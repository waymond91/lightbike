#! /home/brett/anaconda3/envs/keras/bin/python
import numpy as np
import random
import seaborn as sns
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
from matplotlib import colors
import pickle
from progress.bar import IncrementalBar
import copy
import a_star
matplotlib.use('Qt5Agg')
import sys
import qtgui
from PyQt5 import QtWidgets


class checker:
    def __init__(self, key):
        self.key = key
        self.last = None

    def update(self, state):
        # print(state.keys())
        my_position = state['player_info'][self.key]['position']
        num_players = state['game_info']['num_players']
        x_bound = state['game_info']['x_bound']
        y_bound = state['game_info']['y_bound']

        board = np.full((y_bound + 2, x_bound + 2), np.nan)

        for player in state['player_info'].values():
            for location in player['locations']:
                board[location[1], location[0]] = 1

        for x in range(0, x_bound + 2):
            board[0, x] = 1
            board[y_bound + 1, x] = 1
        for y in range(0, y_bound + 2):
            board[y, 0] = 1
            board[y, x_bound + 1] = 1

        pos_right = (my_position[0] + 1, my_position[1])
        pos_left = (my_position[0] - 1, my_position[1])
        pos_down = (my_position[0], my_position[1] - 1)
        pos_up = (my_position[0], my_position[1] + 1)

        up = board[pos_up[::-1]]
        down = board[pos_down[::-1]]
        left = board[pos_left[::-1]]
        right = board[pos_right[::-1]]

        possible_choices = []
        choices_locations = []
        if up not in [-1, 1]:
            possible_choices.append('up')
            choices_locations.append(pos_up)
        if down not in [-1, 1]:
            possible_choices.append('down')
            choices_locations.append(pos_down)
        if left not in [-1, 1]:
            possible_choices.append('left')
            choices_locations.append(pos_left)
        if right not in [-1, 1]:
            possible_choices.append('right')
            choices_locations.append(pos_right)

        if possible_choices:
            ret = random.choice(possible_choices)
        else:
            # print(self.name+' Trapped!')
            ret = 'up'

        to_plot = None
        if choices_locations and self.key == 'player_0':
            to_plot = {
                'checker choices': {
                    'color': 'gold',
                    'locations': choices_locations,
                }
            }
            # print(to_plot)
        """
        if to_plot:
            game_plot(state, plot_objects=to_plot)
        """
        self.last = ret
        return (ret)


class lightbike:
    dead = False

    opposites = {
        'up': 'down',
        'down': 'up',
        'left': 'right',
        'right': 'left',
    }

    def __init__(self, bot_object, starting_location):
        self.player_update = bot_object.update
        self.location = starting_location
        self.direction = None

    def update(self, state):
        self.last_direction = self.direction
        self.direction = self.player_update(state)

        if self.last_direction == self.opposites[self.direction]:
            self.direction = self.last_direction

        return (self.direction)


class lightbike_fight:
    def __init__(self, bots, x, y):
        self.count = 0
        self.x_bound = x
        self.y_bound = y
        self.players = {}
        self.state = {'game_info': {}, 'player_info': {}}
        self.states = []
        self.results = {}
        self.losers = []
        self.blocked_locations = []
        self.num_players = len(bots)

        self.state['game_info'].update({
            'x_bound': self.x_bound,
            'y_bound': self.y_bound,
            'num_players': self.num_players,
            'move_no': 0,
        })

        for bot, bot_function in bots.items():
            x_start = random.randint(2, self.x_bound-1)
            y_start = random.randint(2, self.y_bound-1)

            self.players.update({
                bot: lightbike(bot_function, [x_start, y_start])
            })

            self.state['player_info'].update({
                bot: {
                    'locations': [],
                    'position': [x_start, y_start],
                }
            })

    def update(self):
        for player_key, player in self.players.items():
            if player.dead == False:
                player.update(self.state)
                if player.direction == 'up':
                    player.location[1] += 1
                elif player.direction == 'down':
                    player.location[1] -= 1
                elif player.direction == 'right':
                    player.location[0] += 1
                elif player.direction == 'left':
                    player.location[0] -= 1
        for player_key, player in self.players.items():
            if player.dead == False:
                self.state['player_info'][player_key]['position'] = player.location
                self.state['player_info'][player_key]['locations'].append(player.location[:])

        for player in self.state['player_info'].values():
            if len(player['locations']) > 1:
                self.blocked_locations.append(copy.deepcopy(player['locations'][-2]))
        self.check_for_losers()
        self.state['game_info']['move_no'] += 1
        self.states.append(copy.deepcopy(self.state))

    def check_for_losers(self):
        rank = self.num_players - len(self.losers)

        # Check for draws
        positions = {}
        for key, player in self.players.items():
            positions.update({
                key: player.location
            })

        # reverse dict and check for duplicates:
        flipped = {}
        for key, value in positions.items():
            value = tuple(value)
            if value not in flipped:
                flipped[value] = [key]
            else:
                flipped[value].append(key)
        if len(flipped) < self.num_players:
            for loc_key, players in flipped.items():
                if len(players) > 1:
                    for player in players:
                        p = self.players[player]
                        if p.dead == False:
                            p.dead = True
                            self.losers.append(player)
                            self.results.update({
                                player: {
                                    'rank': rank,
                                    'moves': self.count,
                                }
                            })

        for player_key, player in self.players.items():
            if player.dead == False:
                if player.location[0] > self.x_bound or player.location[0] < 0:
                    player.dead = True
                if player.location[1] > self.y_bound or player.location[1] < 0:
                    player.dead = True

                if player.location in self.blocked_locations:
                    # print("tile blocked.")
                    player.dead = True

                if player.dead == True:
                    if player_key not in self.losers:
                        self.losers.append(player_key)
                        self.results.update({
                            player_key: {
                                'rank': rank,
                                'moves': self.count,
                            }
                        })

        self.count += 1


def game_plot(state, plot_objects=None):
    player_colors = [
        'indianred',
        'maroon',
        'darkorange',
        'darkolivegreen',
        'lightseagreen',
        'darkslategray',
        'rebeccapurple',
    ]

    rendered_colors = ['black', 'floralwhite']
    rendered_objects = ['Game Border', 'Open Square']

    num_objects = 2 + state['game_info']['num_players']
    y_bound = state['game_info']['y_bound']
    x_bound = state['game_info']['x_bound']

    img = np.full((y_bound + 2, x_bound + 2), 1)

    for x in range(0, x_bound + 2):
        img[0, x] = 0
        img[y_bound + 1, x] = 0
    for y in range(0, y_bound + 2):
        img[y, 0] = 0
        img[y, x_bound + 1] = 0

    color_index = 2

    for player_key, player in state['player_info'].items():
        for location in player['locations'][:-1]:
            img[location[1], location[0]] = color_index
        position = player['position']
        img[position[1], position[0]] = color_index
        rendered_colors.append(player_colors[color_index])
        rendered_objects.append(player_key)
        color_index += 1

    if plot_objects:
        num_objects += len(plot_objects)
        for key, values in plot_objects.items():
            # print(key, values)
            rendered_colors.append(values['color'])
            rendered_objects.append(key)
            for location in values['locations']:
                img[location[1], location[0]] = color_index
            color_index += 1

    plt.figure(figsize=(6, 6))
    cmap = colors.ListedColormap(rendered_colors)
    bounds = np.linspace(0, num_objects, num_objects + 1)
    # bounds=[-0.5, 0.5, 1.5, 2.5, 3.5]

    norm = colors.BoundaryNorm(bounds, cmap.N)
    # heatmap = plt.pcolor(np.array(img), cmap=cmap, norm=norm)
    # plt.colorbar(ax, ticks=[0, 1, 2, 3])

    ax = sns.heatmap(img, cmap=cmap, norm=norm, annot=False, linewidths=.06250, )
    patches = []
    for player in state['player_info'].values():
        # print(player)
        patches.append(Rectangle(player['position'], 1, 1, fill=False, edgecolor='black', lw=3))
        # patches.append(player['position'])
    for patch in patches:
        ax.add_patch(patch)

    cbar = ax.collections[0].colorbar
    cbar.set_ticks(bounds + 0.5)
    cbar.set_ticklabels(rendered_objects)
    ax.invert_yaxis()
    plt.show()


def test_bots(bot_objects, num_games, x, y):
    games = []
    ranks = []
    states = []
    results = []

    for i in range(num_games):
        games.append(lightbike_fight(bot_objects, x, y))

    user_objects = {k: [] for k in list(bot_objects.keys())}
    plot_objects = [copy.deepcopy(user_objects)for _ in games]

    bar = IncrementalBar('Computing Game Outcomes', max=len(games))
    for i, game in enumerate(games):
        while len(game.losers) < game.num_players:
            game.update()
            for bot_name, bot in bot_objects.items():
                try:
                    x = (bot_name, bot.plot_objects)
                    user_spots = bot.plot_objects
                    plot_objects[i][bot_name].append(copy.deepcopy(user_spots))

                except AttributeError:
                    #print('Bot has no plot objects. Passing')
                    pass
        ranks.append(game.results)
        states.append(game.states)
        bar.next()
    bar.finish()
    for i in range(len(games)):
        results.append({
            'ranks': ranks[i],
            'states': states[i],
            'user_plots': plot_objects[i],
        })

    return results


def display_results(results_file):
    qapp = QtWidgets.QApplication(sys.argv)
    app = qtgui.ApplicationWindow(results_file)
    app.show()
    qapp.exec_()


if __name__ == '__main__':
    bots = {
        'a_star': a_star.a_star,
        'player0': checker('player0').update,
        'player1': checker('player1').update,
        'player2': checker('player2').update,
        'player3': checker('player3').update,
        'player4': checker('player4').update,
    }

    results = test_bots(bots, 50, 20, 20, plot=True)

    pickle.dump(results, open("results.p", "wb"))
