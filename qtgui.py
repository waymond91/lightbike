"""
Modifications have been made to LegendItem.py from pyqtgraph in order to properly render game legends.
"""

import sys
import pickle
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5 import QtWidgets
from PyQt5.QtCore import *
import pyqtgraph as pg
import seaborn as sns
import copy

class lb_plot:
    def __init__(self, states):
        self.last_index = 0
        self.x_bound = states[0]['game_info']['x_bound']
        self.y_bound = states[0]['game_info']['y_bound']
        self.num_players = states[0]['game_info']['num_players']
        self.grid = False

        self.colors = {}
        pal = sns.color_palette("Paired", )
        for i, color in enumerate(pal.as_hex()):
            self.colors.update({
                i: color
            })

        self.border_color = 'w'
        self.border = []
        for x in range(-1, self.x_bound + 2):
            self.border.append(self.get_spot(-1, x, self.border_color))
            self.border.append(self.get_spot(self.y_bound + 1, x, self.border_color))

        for y in range(-1, self.y_bound + 2):
            self.border.append(self.get_spot(y, -1, self.border_color))
            self.border.append(self.get_spot(y, self.x_bound + 1, self.border_color))

    def get_spot(self, y, x, color):
        if self.grid == False:
            spot = {
                'pos': (y, x),
                'pen': {'color': color, 'width': 0},
                'brush': color,
            }
        else:
            spot = {
                'pos': (y, x),
                'pen': {'color': 'k', 'width': 0},
                'brush': color,
            }
        return spot

    def get_next(self, states, i):
        spots = []
        for j, player in enumerate(states[i]['player_info'].values()):
            position = player['position']
            spots.append(self.get_spot(position[1], position[0], self.colors[2 * j + 1]))
            if len(player['locations']) > 1:
                location = player['locations'][-2]
                spots.append(self.get_spot(location[1], location[0], self.colors[2 * j]))
        self.last_index = i
        return spots

    def get_board(self, states, i):
        spots = []
        for state in states[0:i + 1]:
            for i, player in enumerate(state['player_info'].values()):
                position = player['position']
                spots.append(self.get_spot(position[1], position[0], self.colors[2 * i + 1]))

                if len(player['locations']) > 1:
                    location = player['locations'][-2]
                    spots.append(self.get_spot(location[1], location[0], self.colors[2 * i]))

        return spots


class App(QWidget):

    def __init__(self):
        super().__init__()
        self.title = 'Unnatural Selection'
        self.left = 10
        self.top = 10
        # self.width = 600
        # self.height = 1200
        self.initUI()


class ApplicationWindow(QtWidgets.QMainWindow):
    def __init__(self, results):
        try:
            self.results = pickle.load(open('results.p', 'rb'))
        except FileNotFoundError:
            print("Results file not found. This will crash the program.")

        super().__init__()
        # pg.setConfigOption('background', 'w')
        # pg.setConfigOption('foreground', 'k')
        self._main = QtWidgets.QWidget()
        self.setCentralWidget(self._main)
        self.setStyleSheet(open('qtdark.qss').read())

        self.state_index = 0
        self.last_state_index = 0
        self.game_index = 0
        self.states = self.results[self.game_index]['states']

        # Create the view
        self.view = pg.PlotWidget()
        self.view.resize(800, 1200)
        self.view.setWindowTitle('Unnatural Selection')
        self.view.setAspectLocked(True)
        self.view.show()

        self.l1 = QLabel()
        #self.l1.setAlignment(Qt.AlignRight)
        self.l1.setText("Move No: " + str(self.state_index))

        self.si = QSpinBox()
        self.si.setMinimum(0)
        self.si.setMaximum(len(self.states) - 1)
        self.si.setSingleStep(1)
        self.si.valueChanged.connect(self.si_handler)

        self.l2 = QLabel()
        self.l2.setAlignment(Qt.AlignRight)
        #self.l2.setText("Game No: " + str(self.game_index))


        self.gi = QSpinBox()
        self.gi.setMinimum(1)
        self.gi.setMaximum(len(self.results))
        self.gi.setSingleStep(1)
        self.gi.valueChanged.connect(self.gi_handler)

        self.gt = QCheckBox()
        self.gt.setText("Toggle Grid")
        self.gt.toggled.connect(self.gt_handler)

        self.upt = QCheckBox()
        self.upt.setText("Toggle User Plot")
        self.upt.toggled.connect(self.upt_handler)
        self.upt_value = False


        self.l3 = QLabel()
        self.l3.setText("Playback Speed")
        #self.l3.setAlignment(Qt.AlignRight)

        self.ss = QSlider()
        self.ss.setOrientation(Qt.Horizontal)
        self.ss.setMinimum(0)
        self.ss.setMaximum(1000)
        self.ss.setTickInterval(10)
        self.ss.setValue(1000)
        self.ss.valueChanged.connect(self.ss_handler)

        self.playing = False
        self.pb = QPushButton()
        self.pb.setText('Play')
        self.pb.clicked.connect(self.pb_handler)

        self.timer = QTimer()
        self.timer.timeout.connect(self.timer_handler)

        # Create the scatter plot and add it to the view
        self.scatter = pg.ScatterPlotItem(pxMode=False, size=1, symbol='s')
        self.view.addItem(self.scatter)
        self.index = 0
        self.lb = lb_plot(self.results[0]['states'])
        self.scatter.addPoints(self.lb.border)

        self.legend = self.view.addLegend()
        for i, player in enumerate(self.states[0]['player_info']):
            pdi = pg.PlotDataItem([4, 3, 2, 1], pen=None, symbol='s', symbolPen=self.lb.colors[2 * i + 1],
                                  symbolBrush=self.lb.colors[2 * i])
            self.legend.addItem(pdi, player)

        layout = QGridLayout(self._main)
        layout.addWidget(self.view, 0, 0, 1, 0)
        layout.addWidget(self.l1, 1, 0)
        layout.addWidget(self.si, 1, 1)
        layout.addWidget(self.l2, 1, 2)
        layout.addWidget(self.gi, 1, 3)
        layout.addWidget(self.pb, 3, 0, 1, 4)
        layout.addWidget(self.l3, 4, 0, 1, 1, alignment=Qt.AlignHCenter)
        layout.addWidget(self.ss, 4, 1, 1, 3)
        layout.addWidget(self.gt, 5, 0, 1, 2, alignment=Qt.AlignHCenter)
        layout.addWidget(self.upt,5, 2, 1, 2, alignment=Qt.AlignHCenter)


    def ss_handler(self):
        if self.playing == True:
            self.timer.start(1000 - self.ss.value())

    def upt_handler(self):
        self.upt_value = not self.upt_value
        if self.upt_value == False:
            for user in self.results[self.game_index]['user_plots'].values():
                try:
                    erasers = []
                    for last_value in user[self.state_index+1]:
                        last_spot = copy.deepcopy(last_value)
                        last_spot['pen']['color'] = 'k'
                        last_spot['brush'] = 'k'
                        erasers.append(last_spot)
                    self.scatter.addPoints(erasers)
                except IndexError:
                    pass
        self.update()

    def gt_handler(self):
        self.lb.grid = not (self.lb.grid)
        self.reset()
        spots = self.lb.get_board(self.results[self.game_index]['states'], self.state_index)
        self.scatter.addPoints(spots)

    def reset(self):
        self.scatter.clear()
        self.scatter.addPoints(self.lb.border)

    def update(self):
        diff = self.state_index - self.last_state_index
        if diff == 1:

            # Add user objects
            if self.upt_value:
                for user in self.results[self.game_index]['user_plots'].values():
                    try:
                        erasers = []
                        for last_value in user[self.state_index]:
                            last_spot = copy.deepcopy(last_value)
                            last_spot['pen']['color']='k'
                            last_spot['brush'] = 'k'
                            erasers.append(last_spot)
                        self.scatter.addPoints(erasers)

                        user_spots = user[self.state_index+1]
                        self.scatter.addPoints(user_spots)
                        # remove last user_plot

                    except IndexError:
                        #print(user + " has no plot objects")
                        pass

            # Add bikes:
            spots = self.lb.get_next(self.results[self.game_index]['states'], self.state_index)
            self.scatter.addPoints(spots)

        else:
            self.reset()
            spots = self.lb.get_board(self.results[self.game_index]['states'], self.state_index)
            self.scatter.addPoints(spots)
            if self.upt_value:
                for user in self.results[self.game_index]['user_plots'].values():
                    try:
                        user_spots = user[self.state_index+1]
                        self.scatter.addPoints(user_spots)
                    except IndexError:
                        pass

    def si_handler(self):
        self.last_state_index = self.state_index
        self.state_index = self.si.value()
        self.l1.setText("Move No: " + str(self.state_index))
        self.update()

    def gi_handler(self):
        self.game_index = self.gi.value() -1
        print(self.results[self.game_index]['ranks'])
        self.si.setValue(0)
        self.states = self.results[self.game_index]['states']
        self.reset()
        self.si.setMaximum(len(self.states) - 1)
        self.l2.setText("Game No: " + str(self.game_index))
        self.update()

    def pb_handler(self):
        if self.playing:
            self.playing = False
            self.timer.stop()
            self.pb.setText('Play')
        else:
            if self.si.value() >= len(self.states) - 1:
                self.si.setValue(0)
            self.playing = True
            self.timer.start(1000 - self.ss.value())
            self.pb.setText('Pause')

    def timer_handler(self):
        # print(self.si.value(), len(self.states))
        self.si.setValue(self.si.value() + 1)
        self.update()
        if self.si.value() >= len(self.states) - 1:
            # print('stopped')
            self.timer.stop()
            self.pb.setText('Play')
            self.playing = False


if __name__ == '__main__':
    qapp = QtWidgets.QApplication(sys.argv)
    app = ApplicationWindow()
    app.show()
    qapp.exec_()
